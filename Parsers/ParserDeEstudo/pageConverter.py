# encoding: utf-8
# encoding: iso-8859-1
# encoding: win-1252
import json
import sys
import codecs
import re
import xml.etree.ElementTree as ET
from datetime import datetime
from xml.etree import ElementTree
from xml.dom import minidom

def prettify(elem):
    """Return a pretty-printed XML string for the Element.
    """
    # root = ET.parse('thefile.xml').getroot()
    rough_string = ElementTree.tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="  ")



def myReplace(filename, replacePattern):
	contador = 0
	begin  = datetime.now()	
	newfilename  = "new_"+ filename.split('.')[0] + ".xml"
	
	with codecs.open(replacePattern, 'r', encoding='utf-8') as f:
	    replacePattern_json = json.load(f)

	with codecs.open(filename, 'r', encoding='latin-1') as f:
			filedata = f.read()

   	#tratando corpo do arquiv
	for pattern in replacePattern_json['padrao']:
		print("\033[1;32;40m" + pattern['buscar'] , "\033[1;32;40m" + pattern['substituirPor'])
		print ('\033[1;32;40m + este padrão acima sera compilado agora')
		print("\033[0;37;40m . \n")
		buscarPor = re.compile(pattern['buscar'],re.MULTILINE)
		
		filedata = re.sub(buscarPor ,pattern['substituirPor'], filedata, pattern['repetir'])
		contador = contador + 1
		newfile = codecs.open (newfilename,"w+", encoding='utf-8')
		newfile.write(filedata)
		newfile.close()
	
		#if input('Do You Want To Continue? ') != 'y':
		#	sys.exit()
   	
   	#inserindo cabeçalho
	with open(newfilename, 'r+', encoding='utf-8' ) as f:
 		content = f.read()
 		f.seek(0, 0)
 		f.write(replacePattern_json['complementoXML'][0]['cabecalho'].rstrip('\r\n') + '\n' + content)
 		f.close()
 	
 	#tagueando a epigrafe 
#	with open(newfilename,'r+', encoding='utf-8') as f:
#		head = [next(f) for x in range(40)]
#		str1 = ""
#		head = str1.join(head) 
#		epigrafe = re.search("^[A-Z]{3,}[A-ZÁÉÓÚÃẼÕÂÊÔÇÀ0-9\\s ]{7,}", head, re.M ).group(0)
#		epigrafeTag = '<epigrafe>'+epigrafe+'</epigrafe>'
#		head = re.sub(epigrafe,epigrafeTag,head, re.S )
#		filedata = head + f.read()
#		f.seek(0, 0)
##		f.write(filedata)
#		f.close()




	#inserindo rodape
	with open("new_"+ filename.split('.')[0] + ".xml", 'a',  encoding='utf-8' ) as f:
		f.write('\r\n'+replacePattern_json['complementoXML'][0]['rodape'].rstrip('\r\n'))
		f.close()

	#contador tempo
	end =  datetime.now()	
	timeelapsed = end - begin
	print(begin,end)
	print("tempo decorrido em segundos")
	print(timeelapsed.seconds)


	#ajuste


def checkXMLGenerate(filename):
	print ('oi')

def checkWellFormed(filename):
	print ('oi')

def fixXMLBadFormed(filename):
	print ('oi')

def generateHTMLfromXML(filename):
	print ('oi')


if __name__== "__main__":
	myReplace(sys.argv[1], sys.argv[2])
	



